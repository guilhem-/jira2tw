import subprocess
import platform
import os
import shutil

class ChromeOpener:
    def __init__(self):
        # Determine the location of the Chrome executable based on the operating system
        system = platform.system()
        if system == 'Windows':
            self.chrome_exe = 'chrome.exe'
        elif system == 'Linux':
            self.chrome_exe = 'google-chrome'
        elif system == 'Darwin':
            self.chrome_exe = 'google chrome'
        else:
            raise Exception("Unsupported operating system")
        
        # Check if Chrome is in the system's PATH, and if not, search for it
        if not self.is_chrome_in_path():
            self.chrome_exe = self.find_chrome_executable()

    def open_url(self, url):
        try:
            # Run Chrome with the provided URL
            subprocess.Popen([self.chrome_exe, url])
            print(f"Opening {url} in Google Chrome...")
        except Exception as e:
            print(f"Error: {e}")

    def open_relativeurl(self, relativeurl):
        try:
            # Convert the relative URL to an absolute file path
            absolute_path = os.path.abspath(relativeurl)
            
            # On Windows, replace backslashes with forward slashes for URL format
            if platform.system() == 'Windows':
                absolute_path = absolute_path.replace("\\", "/")
            
            # Construct the file:// URL
            url = f"file://{absolute_path}"

            # Open the file in Google Chrome
            self.open_url(url)
        except Exception as e:
            print(f"Error: {e}")

    def is_chrome_in_path(self):
        # Check if the Chrome executable is in the system's PATH
        return shutil.which(self.chrome_exe) is not None

    def find_chrome_executable(self):
        # Search for the Chrome executable in common installation locations
        possible_locations = [
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",  # Windows
            "/usr/bin/google-chrome",                                     # Linux
            "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"  # macOS
        ]

        for location in possible_locations:
            if os.path.isfile(location):
                return location

        raise Exception("Chrome executable not found. Please install Chrome or specify the path.")

# Example usage:
if __name__ == "__main__":
    chrome_opener = ChromeOpener()
    
    # Example for opening a local HTML file (change the path to your file)
    chrome_opener.open_relativeurl("path/to/your/local/file.html")
