import json
import requests
import base64
from datetime import datetime
import re


class TiddlerImageSaver:
    def generate_tiddlywiki_date(self):
        now = datetime.utcnow()
        return now.strftime("%Y%m%d%H%M%S") + "{:03d}".format(now.microsecond // 1000)
    def add_slashes(self,s):
        return re.sub(r'([\\\"])', r'\\\1', s).replace('\0', '\\0')
    def save_image_to_tiddler(self, image_url, tiddler_title="title", tiddler_tags=[]):
        # Download the image from the URL
        image_type = None
        tiddler_image_type = "image/unknown"
        response = requests.get(image_url)
        if response.status_code != 200:
            print(f"Failed to download image from {image_url}")
            return
        
        content_type = response.headers.get('Content-Type')
        if content_type:
            tiddler_image_type = content_type.split(';')[0]  # Extracts 'image/svg+xml' from 'image/svg+xml;charset... ', for example
            image_type = content_type.split('/')[-1]  # Extracts 'jpeg' from 'image/jpeg', for example
            print(f"Image type is: {image_type}")
        else:
            print("Content-Type header is missing")

        # Encode the image data as base64

        # Create the tiddler content with the embedded image
        tiddler_content = {}
        
        TDnow=self.generate_tiddlywiki_date()
        tiddler_content['created']=TDnow;
        tiddler_content['modified']=TDnow;
        if tiddler_image_type=="image/svg+xml":
            tiddler_content['text']=f"\"{(json.dumps(response.content.decode('utf-8')))}\""
            tiddler_content['text']='²-*'+json.dumps(response.content.decode('utf-8'))
        
        else:
            image_data_base64 = base64.b64encode(response.content).decode("utf-8")
            tiddler_content['text']=image_data_base64;
        tiddler_content['type']=tiddler_image_type;
        tiddler_content['title']=tiddler_title;
        if len(tiddler_tags):
            tiddler_content['tags']=json.dumps(tiddler_tags);
        return tiddler_content
