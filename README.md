# jira2tw

A basic proof of concept to convert a Jira project to a single [Tiddlywiki](https://tiddlywiki.com/) file.

## License

This project is licensed under [MIT license](LICENSE.txt).


## Usage/Examples

Add requirements to your venv.

Edit mig.py line 100 to define your target, credentials before running the script.

```shell
python mig.py
```


## TODO

Tests

Config management through command line / config file

Attached document management
