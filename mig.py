from jira import JIRA, Comment 
from datetime import datetime, timedelta
from chromeopener import ChromeOpener
from tiddlerimagesaver import TiddlerImageSaver
import json
import os
import pytz
import re
import shutil

def convert_to_utc(input_date):
    # Parse the input date string
    date_format = "%Y-%m-%dT%H:%M:%S.%f%z"
    parsed_date = datetime.strptime(input_date, date_format)

    # Convert to UTC
    utc_date = parsed_date.astimezone(pytz.utc)

    # Format the date in the specified format (YYYY0MM0DD0hh0mm)
    output_format = "%Y%m%d%H%M"
    return utc_date.strftime(output_format)

def copy_html_file():

    source_path = os.path.join(os.getcwd(), 'twsrc', 'empty.html')
    destination_path = os.path.join(os.getcwd(), 'tw.html')    

    try:
        shutil.copyfile(source_path, destination_path)
        print(f"File '{source_path}' copied to '{destination_path}' successfully.")
    except FileNotFoundError:
        print(f"File '{source_path}' not found.")
    except Exception as e:
        print(f"An error occurred: {e}")

# Call the function to copy the HTML file
copy_html_file()

EXCLUDETOKEN='²-*'
# Custom JSONEncoder to preserve Unicode escape sequences
class UnicodeEscapeJSONEncoder(json.JSONEncoder):
    def encode(self, o):
        return self.raw_encode(o)

    def add_slashes(self,s):
        return re.sub(r'([\\\"])', r'\\\1', s).replace('\0', '\\0')

    def raw_encode(self, o):
        if isinstance(o, str):
            if not o.startswith(EXCLUDETOKEN):
                x=self.add_slashes(o)
                return '"'+''.join(self.custom_encode(c) for c in x)+'"'
            else:
                x=str(o).lstrip(EXCLUDETOKEN)
                return ''.join(self.custom_encode(c) for c in x)
        elif isinstance(o, list):
            return '[' + ', '.join(self.raw_encode(item) for item in o) + ']'
        elif isinstance(o, dict):
            return '{' + ', '.join(f'"{key}": {self.raw_encode(value)}' for key, value in o.items()) + '}'
        elif isinstance(o, int):
            return f'"{o}"'
        else:
            return json.JSONEncoder.default(self, o)

    def custom_encode(self, c):
        if c == '<':
            return r'\u003C'

        return c

 
# Recursive function to convert all values to strings in a JSON structure
def convert_values_to_strings(data):
    if isinstance(data, dict):
        return {key: convert_values_to_strings(value) for key, value in data.items()}
    elif isinstance(data, list):
        return [convert_values_to_strings(item) for item in data]
    elif isinstance(data, str):
        return data
    else:
        return str(data)

def transform_value(key, value):
    if key == 'body':
        return value
    elif key == 'author':
        ks=['accountId','accountType','active','displayName', 'emailAdress']
        if not getattr(identities, value.accountId, False):
            identities[value.accountId]={k:str(getattr(value,k,None)) for k in ks }
        return value.displayName+"|"+value.accountId #displayName
    elif key == 'created':
        return convert_to_utc(value)
    return value  # Default case if no transformation is needed


identities={}
issuetypestruct={}        
# Set up your Jira credentials and project details
jira_url = "https://YOURPROJECT.atlassian.net/"
jira_username = "YOURMAIL@HERE.com"
jira_username = "GUESSWHAT"
jira_password = "YOURPASSWORD"
project_key = "SCR" 
jira_mail = "YOURMAIL@HERE.com"
jira_token="YOURTOKEN"
# Connect to Jira
##jira = JIRA(server=jira_url, basic_auth=(jira_username, jira_password))
jira = JIRA(server=jira_url, basic_auth=(jira_mail, jira_token))

# Retrieve issues from the Jira project
issues = jira.search_issues(f'project={project_key}')

# Path to your TiddlyWiki HTML file
tiddlywiki_file_path = "tw.html"

# Load the HTML file containing the TiddlyWiki data
with open(tiddlywiki_file_path, "r", encoding="utf-8") as tiddlywiki_file:
    html_content = tiddlywiki_file.read()

# Find the <script> tag containing the TiddlyWiki data
START_JSON='<script class="tiddlywiki-tiddler-store" type="application/json">'
start_index = html_content.find(START_JSON)
end_index = html_content.find('</script>', start_index)

if start_index != -1 and end_index != -1:
    start_index+=len(START_JSON)
    # Extract the JSON data from the <script> tag
    tiddler_store_json = html_content[start_index:end_index]
    # Parse the JSON data
    tiddler_store_data = json.loads(tiddler_store_json)
    
    # Function to convert datetime to TiddlyWiki date format
    def convert_to_tiddlywiki_date(date_str):
        # Parse the date string
        date_format = "%Y-%m-%dT%H:%M:%S.%f%z"
        parsed_date = datetime.strptime(date_str, date_format)
        
        # Calculate the UTC offset in minutes
        utc_date = parsed_date - parsed_date.utcoffset()
        # Format the date in TiddlyWiki format
        tiddlywiki_date = utc_date.strftime(f"%Y%m%d%H%M%S%f")
        
        return tiddlywiki_date

        # Function to retrieve additional Jira fields and update the Tiddler
    def update_tiddler_with_jira_data(issue, tiddler):

        if hasattr(issue.fields, 'epic'):
            tiddler["issue_epic_key"]  = issue.fields.epic.key

    # Iterate over the issue fields dictionary
        for field, value in issue.fields.__dict__.items():
            if field.startswith('aggregateprogress'): continue
            if field.startswith('customfield'): continue
            if field.startswith('worklog'): continue
            if field.startswith('key'): 
                continue
            if field.startswith('watches'): continue
            if field.startswith('issuerestriction'): continue
#            if field.startswith('comment'): 
#                continue
            if field== "comment":
                # Create an array to store comment structures
                if len(value.comments)<=0: continue
                if all(isinstance(item, Comment) for item in value.comments):
                    # Extract comment text from comment objects
                    keys = ['body', 'author', 'created']

                    comments_structure = [{key: transform_value(key, getattr(comment,key, None)) for key in keys} for comment in value.comments]

                    # Convert the structure to JSON and set it in the tiddler
                    tiddler[f"issue_{field}"] = json.dumps(comments_structure)
                continue
            print(f"Field: {field}, Value: {value}\n")
            if value is not None:
                # Check if the value is a list (array)
                if isinstance(value, list):
                    # Convert the list to JSON and set it in the tiddler
                    if field== "subtasks":
                       if len(value):
                          tiddler[f"issue_{field}"] = json.dumps([f"{x.key}" for x in value])
                    else:
                       tiddler[f"issue_{field}"] = json.dumps(value)
                      
                else:
                # Set the value as-is in the tiddler
                    if field== "description":
                        tiddler[f"issue_{field}"] =  EXCLUDETOKEN+json.dumps(str(value))
                    elif field== "timetracking":
                        tiddler[f"issue_{field}"] = value.remainingEstimate if value.remainingEstimate is not None else ""
                    elif field== "agregateprogress":
                        tiddler[f"issue_{field}"] = str(value.progress)
                        tiddler[f"issue_{field}total"] = str(value.total)
                    elif field== "progress":
                        tiddler[f"issue_{field}"] = str(value.progress)
                        tiddler[f"issue_{field}total"] = str(value.total)
                    elif field== "votes":
                        tiddler[f"issue_{field}"] = value.votes
                    elif field== "assignee":
                        tiddler[f"issue_{field}"] = value.displayName if value.displayName is not None else "Unassigned"
                    elif field== "creator":
                        tiddler[f"issue_{field}"] = value.displayName
                        tiddler[f"issue_{field}mail"] = value.emailAddress
                    elif field== "reporter":
                        tiddler[f"issue_{field}"] = value.displayName
                        tiddler[f"issue_{field}mail"] = value.emailAddress
                    elif field== "issuetype":
                        tiddler[f"issue_{field}"] = value.name
                        issuetypestruct[value.name]=value.iconUrl
                    elif field== "priority":
                        tiddler[f"issue_{field}"] = value.name
                        tiddler[f"issue_{field}id"] = value.id
                    elif field== "status":
                        tiddler[f"issue_{field}"] = value.name
                        tiddler[f"issue_{field}id"] = value.id
                    elif field== "project":
                        tiddler[f"issue_{field}name"] = value.name
                        tiddler[f"issue_{field}key"] = value.key
                        tiddler[f"issue_{field}id"] = value.id
                    elif isinstance(value, dict):
                        if value.hasattr(field, 'name'):
                            print('X' * 10+value.name)
                            tiddler[f"issue_{field}"] = value.name
                        else:
                            tiddler[f"issue_{field}"] = "QQQ"
                    elif isinstance(value, str):
                        tiddler[f"issue_{field}"] = value
                    else:
                        tiddler[f"issue_{field}"] = str(value)

            else:
                # Set an empty string for None values
                tiddler[f"issue_{field}"] = "None"
        tiddler["issue_key"]=issue.key
        tiddler["title"]= f'{issue.key} - {issue.fields.summary}'
        tiddler["alias"]= f'{issue.key}'
        tiddler["modified"] = convert_to_tiddlywiki_date(issue.fields.updated)  
        tiddler["created"] = convert_to_tiddlywiki_date(issue.fields.created)
        tiddler["tags"] = ' '.join('[['+l+']]' for l in issue.fields.labels) # Populate "tags" field using labels        
        tiddler["text"]= r'{{||render}}'

        return tiddler

    def update_tiddler_with_jira_data2(issue, tiddler):
        # Retrieve Jira fields
        #tiddler["type"] = "text/html" #issue.fields.issuetype.name

        tiddler["issue_key"] = issue.key  if issue.fields.summary is not None else ""
        tiddler["issue_summary"] = issue.fields.summary if issue.fields.summary is not None else ""
        tiddler["issue_description"] = issue.fields.description if issue.fields.description is not None else ""
        tiddler["issue_status"] = issue.fields.status.name if issue.fields.summary is not None else ""
        tiddler["issue_priority"] = issue.fields.priority.name if issue.fields.summary is not None else ""
        tiddler["issue_assignee"] = issue.fields.assignee.displayName if hasattr(issue.fields, 'assignee') and issue.fields.assignee else "Unassigned"
        tiddler["issue_reporter"] = issue.fields.reporter.displayName if hasattr(issue.fields, 'reporter') and issue.fields.reporter else "Unknown"
        tiddler["issue_labels"] = json.dumps(issue.fields.labels)
        
        tiddler["title"]= f'{issue.key} - {issue.fields.summary}'
        tiddler["modified"] = convert_to_tiddlywiki_date(issue.fields.updated)
        tiddler["created"] = convert_to_tiddlywiki_date(issue.fields.created)
        tiddler["tags"] = ' '.join('[['+l+']]' for l in issue.fields.labels) # Populate "tags" field using labels        
        tiddler["text"]= r'{{||render}}'
        return tiddler
    
    # Loop through Jira issues and add them as Tiddlers to the data
    for issue in issues:

        # Create a new Tiddler with the Jira issue content
        new_tiddler = {}
       # Retrieve additional Jira fields and update the Tiddler
        updated_tiddler = update_tiddler_with_jira_data(issue, new_tiddler)

        # Add the new Tiddler to the TiddlyWiki data
        tiddler_store_data.append(new_tiddler)

    render_tiddler={"title":"render", "text":r'''
    <$view field=modified format=date template="DD mmm YYYY 0hh:0mm" />
    <table class="issue-table">
    <tr>
        <th>Key</th>
        <th>Type</th>
        <th>Summary</th>
        <th>Description</th>
        <th>Status</th>
        <th>Priority</th>
        <th>Assignee</th>
        <th>Reporter</th>
        <th>Labels</th>
    </tr>
        <tr>
        <td>{{!!issue_key}}</td>
        <td><$image source={{{ [{!!issue_issuetype}addprefix[$:/Jresource/]] }}} />
{{!!issue_issuetype}}</td>
        <td>{{!!issue_summary}}</td>
        <td>{{!!issue_description}}</td>
        <td>{{!!issue_status}}</td>
        <td>{{!!issue_priority}}</td>
        <td>{{!!issue_assignee}}</td>
        <td>{{!!issue_reporter}}</td>
        <td>{{!!issue_labels}}</td>
        </tr>
    </table>
    <h2 class="tc-title">
    <!-- check to see if your field issue_subtasks exists and point to sub tasks-->
    <$list filter="[<currentTiddler>has:field[issue_subtasks]]">
        subtasks: 
        <$list variable="index" filter="[<currentTiddler>get[issue_subtasks]jsonindexes[]]">
            <$set name="pre" value={{{ [<currentTiddler>get[issue_subtasks]jsonget<index>addsuffix[ ]] }}}>
            <$list filter="[all[]prefix<pre>get[title]]" variable="tiddlerTitle">
                <$link to=<<tiddlerTitle>>><$text text=<<tiddlerTitle>>/></$link>
            </$list>
            </$set>
        </$list>
    </$list>
    <!-- check to see if your field issue_parent exists and point to parent-->
    <$list filter="[<currentTiddler>has:field[issue_parent]]">  
        parent: 
            <$set name="pre" value={{{ [<currentTiddler>get[issue_parent]addsuffix[ ]] }}}>
            <$list filter="[all[]prefix<pre>get[title]]" variable="tiddlerTitle">
                <$link to=<<tiddlerTitle>>><$text text=<<tiddlerTitle>>/></$link>
            </$list>
            </$set>
    </$list>

    </h2>
<$list filter="[<currentTiddler>get[issue_subtasks]json[]count[]match[0]not[]]">
                    Y A PAS
</$list>    
<!-- Comments -->
<$list filter="[<currentTiddler>get[issue_comment]jsonindexes[]]" variable="index">
<hr width="50%">

<$transclude $variable=linkify text={{{ [<currentTiddler>get[issue_comment]jsonextract<index>jsonget[author]] }}} />
 on 
<$text text={{{ [<currentTiddler>get[issue_comment]jsonextract<index>jsonget[created]parsedate[YYYY0MM0DD0hh0mm]format:date[YYYY-0MM-0DD 0hh:0mm]] }}}/> said :

<$macrocall $name="wrapLines" text={{{[<currentTiddler>get[issue_comment]jsonextract<index>jsonget[body]]}}} />
 
</$list>

    @@background-color:yellow;{{!!issue_key}} {{!!issue_summary}}
    @@'''}
    render_tiddler["text"]=EXCLUDETOKEN+json.dumps(render_tiddler["text"])
    tiddler_store_data.append(render_tiddler)

    renderAuth_tiddler={"title":"renderAuthor", "text":
r'''displayName: {{!!displayName}}

mail: {{!!emailAdress}}

active: {{!!active}}

accountType: {{!!accountType}}

accountId: {{!!accountId}}
'''}
    renderAuth_tiddler["text"]=EXCLUDETOKEN+json.dumps(renderAuth_tiddler["text"])
    tiddler_store_data.append(renderAuth_tiddler)

    macro_tiddler={"tags":"$:/tags/Global","title":"$:/plugins/J/wrapLines", "text":
r'''\define wrapLines(text)
<$list filter="""[[$text$]splitregexp[\n]]""" variable="line" > 

<$text text=<<line>>/>
</$list>
\end
\define linkify(text) 
[[$text$]]
\end'''}
    macro_tiddler["text"]=EXCLUDETOKEN+json.dumps(macro_tiddler["text"])
    tiddler_store_data.append(macro_tiddler)


    parsedate_tiddler={"module-type":"filteroperator","title":"$:/plugins/J/parsedate", "type":"application/javascript","text":
r'''/*\
title: TiddlyTools/Time/ParseDate
type: application/javascript
module-type: filteroperator
original-source: http://TiddlyTools.com
author: EricShulman

''Defines filter operators that convert date-formatted input values to another date format.''

* parsedate[]
  converts date-formatted input values to 17-digit TWCore UTC datetime output
* parsedate[YYYY0MM0DD0hh0mm0ss0XXX]
  converts date-formatted input values to 17-digit TWCore local datetime output
* parsedate[outputformat]
  converts date-formatted input values to TWCore date-formatted text output
* unixtime[] (or parsedate[unixtime])
  converts date-formatted input values to "unix time" signed integer output.
* unixtime[outputformat] (or parsedate:unixtime[outputformat])
  converts "unix time" signed integer input values to TWCore date-formatted text output
* unixtime[YYYY0MM0DD0hh0mm0ss0XXX]
  converts "unix time" signed integer input values to 17-digit TWCore local datetime output
* parsedate:unixtime[]
  converts "unix time" signed integer input values to 17-digit TWCore UTC datetime output

See TiddlyTools/Time/Info for usage details

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

exports.parsedate = function(source,operator,options) {
   var format  = operator.operand || "[UTC]YYYY0MM0DD0hh0mm0ss0XXX", dt, results = [];
   source(function(tiddler,title) {
      if (title.match(/^-?\d+$/)) dt=((operator.suffix=="unixtime")||(operator.suffix=="number"))?new Date(Number(title)):$tw.utils.parseDate(title);
      else                        dt=new Date(title.replace(/(\d+)(st|nd|rd|th)/g,"$1").replace(/,/g,""));
      if (format=="unixtime")    results.push(dt.getTime().toString());
      else if (format=="number") results.push(dt.getTime().toString());
      else                       results.push($tw.utils.formatDateString(dt,format));
   });
   return results;
};

exports.unixtime = function(source,operator,options) {
   var format  = operator.operand || "", dt, results = [];
   source(function(tiddler,title) {
      if (title.match(/^-?\d+$/)) dt=new Date(Number(title));
      else                        dt=new Date(title.replace(/(\d+)(st|nd|rd|th)/g,"$1").replace(/,/g,""));
      if (format=="") results.push(dt.getTime().toString());
      else            results.push($tw.utils.formatDateString(dt,format));
   });
   return results;
};

})();'''}
    parsedate_tiddler["text"]=EXCLUDETOKEN+json.dumps(parsedate_tiddler["text"])
    tiddler_store_data.append(parsedate_tiddler)

    DefaultTiddlers={"title":"$:/DefaultTiddlers" , "text":"[[SCR-1 - toto]][[render]]"}
    DefaultTiddlers["text"]=EXCLUDETOKEN+json.dumps(DefaultTiddlers["text"])
    tiddler_store_data.append(DefaultTiddlers)

    
    issueTiddlers={"title":"All tickets" , "text":'<<list-links "[prefix:title[SCR]]">>'}
    issueTiddlers["text"]=EXCLUDETOKEN+json.dumps(issueTiddlers["text"])
    tiddler_store_data.append(issueTiddlers)

    #images
    for issuetype, url in issuetypestruct.items():  
        print(f"{issuetype}: {url}")
        tid=TiddlerImageSaver().save_image_to_tiddler(url, '$:/Jresource/'+issuetype)
        tiddler_store_data.append(tid)
    #authors
    for id_accountId, id in identities.items():  
        id_tiddler=id;
        id_tiddler.update({"tags":"$:/J/id","title":id_accountId, "text":'{{||renderAuthor}}'})
        id_tiddler["text"]=EXCLUDETOKEN+json.dumps(id_tiddler["text"])
        tiddler_store_data.append(id_tiddler)

    updated_tiddler_store_json = json.dumps(tiddler_store_data, separators=(',', ':'), cls=UnicodeEscapeJSONEncoder)

    # Update the HTML content with the modified JSON data
    html_content = html_content.replace(tiddler_store_json, updated_tiddler_store_json)

    # Write the updated HTML content back to the file
    with open(tiddlywiki_file_path, "wb") as updated_tiddlywiki_file:
        updated_tiddlywiki_file.write(html_content.encode('utf-8'))

print("Jira issues added to TiddlyWiki data successfully.")

chrome_opener = ChromeOpener()
chrome_opener.open_relativeurl(tiddlywiki_file_path)

